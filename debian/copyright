Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: cbm
Upstream-Contact: https://github.com/resurrecting-open-source-projects/cbm/issues
Source: https://github.com/resurrecting-open-source-projects/cbm
Comment:
 2014-02-21 email from Upstream: "there is no more project page, and
 CBM is now completely unmaintained. Feel free to remove it from
 Debian if you see fit." Contact author with "[cbm]" as the first word
 of the subject.
 .
 Personal git at
 https://github.com/aisotton
 .
 2019-02-08, by Eriberto: resurrecting the project in GitHub at
 https://github.com/resurrecting-open-source-projects/cbm

Files: *
Copyright: 2005-2006 Aaron Isotton <aaron@isotton.com>
           2006      Paul Martin <pm@debian.org>
           2007      brian m. carlson <sandals@crustytoothpaste.ath.cx>
           2012      Cyril Brulebois <kibi@debian.org>
           2019-2021 Joao Eriberto Mota Filho <eriberto@eriberto.pro.br>
           2020      daltomi <daltomi@disroot.org>
License: GPL-2

Files: autogen.sh
       doc/create-man.sh
Copyright: 2015-2019 Joao Eriberto Mota Filho <eriberto@eriberto.pro.br>
License: BSD-3-Clause

Files: debian/*
Copyright: 2007-2016 Jari Aalto <jari.aalto@cante.net>
           2008      Moritz Muehlenhoff <jmm@debian.org>
           2014      Ricardo Mones <mones@debian.org>
           2019-2022 Joao Eriberto Mota Filho <eriberto@debian.org>
License: GPL-2+

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in "/usr/share/common-licenses/GPL-2".

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE HOLDERS OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in "/usr/share/common-licenses/GPL-2".
